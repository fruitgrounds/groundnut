var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlantSchema = new Schema({
    name: String,
    _id:String,
    type:String,
    category:[],
    feature:String,
    color:String,
    dimension:{
        height:number,
        width:number
    },
    description:String,
    healthBenefits:[],
    price:number,
    offer:number,
    kind:[],
    review:[{userId:String,
    comments:String,
    rating:String,
    date:Date}],
    unitContains:Number,
    highlights:String,
    available :String,
    image: [],
    created_date: { type: Date, default: Date.now },
    created_by:String,
});

var Plant = mongoose.model('Plant', PlantSchema);

module.exports = Plant;
