var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PotSchema = new Schema({
    name: String,
    _id:String,
    type:String,
    size:String,
    category:[],
    capacity:String,
    drainageHoles:number,
    diameterLower:number,
    diameterHigher:number,
    feature:String,
    color:String,
    dimension:{
        height:number,
        width:number
    },
    unitContains:Number,
    description:String,
    price:number,
    offer:number,
    hasWifi:String,
    additionalFeatures:[],
    review:[{userId:String,
        comments:String,
        rating:String,
        date:Date}],
    placement:String,
    highlights:String,
    available :String,
    image: [],
    created_date: { type: Date, default: Date.now },
    created_by:String,
});

var Pot = mongoose.model('Pot', PotSchema);

module.exports = Pot;
